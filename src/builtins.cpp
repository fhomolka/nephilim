#include "angelscript.h"

#include <string>
#include <iostream>
#include <limits>

void asBox_puts(std::string& str)
{
	std::cout << str;
}

std::string asBox_gets()
{
	std::string str;
	std::cin >> str;
	return str;
}

int asBox_rand()
{
	return rand();
}

void RegisterGlobals(asIScriptEngine* asEngine)
{
	int r;
 	r = asEngine->RegisterGlobalFunction("void print(string& in)", asFUNCTION(asBox_puts), asCALL_STDCALL);	
 	r = asEngine->RegisterGlobalFunction("string getInput()", asFUNCTION(asBox_gets), asCALL_STDCALL);	
 	r = asEngine->RegisterGlobalFunction("int rand()", asFUNCTION(asBox_rand), asCALL_STDCALL);	
}
