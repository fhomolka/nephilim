shared class Person
{
	string Name;

	Person()
	{
		Name = "unknown";
	}

	Person(string name)
	{
		Name = name;
	}

	void Greet(Person @other)
	{
		print("Hello, " + other.Name + "!\n");
	}
} 
