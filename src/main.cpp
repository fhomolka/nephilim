#include <cstring>
#include <iostream>
#include "angelscript.h"
#include "scriptstdstring/scriptstdstring.h"
#include "scriptarray/scriptarray.h"
#include "scriptdictionary/scriptdictionary.h"
#include "scriptmath/scriptmath.h"
#include "scriptfile/scriptfile.h"
#include "scriptbuilder/scriptbuilder.h"


#include "builtins.h"

void MessageCallback(const asSMessageInfo* msg, void* param){
	const char* type = "ERR ";
	if(msg->type == asMSGTYPE_WARNING) type = "WARN";
	else if(msg->type == asMSGTYPE_INFORMATION) type = "INFO";

	std::cout << msg->section << "(" << msg->row << ", " << msg->col << ") : " << type << " : " << msg->message << std::endl;
}

void ConfigEngine(asIScriptEngine* asEngine)
{
	int r;
	asEngine->SetMessageCallback(asFUNCTION(MessageCallback), 0, asCALL_CDECL);
	
	RegisterStdString(asEngine);
	RegisterScriptArray(asEngine, true);
	RegisterScriptDictionary(asEngine);
	RegisterScriptMath(asEngine);
	RegisterScriptFile(asEngine);

	if (!strstr(asGetLibraryOptions(), "AS_MAX_PORTABILITY")) 
	{
		RegisterGlobals(asEngine);
	}
}

int CompileScript(asIScriptEngine* asEngine, std::string& scriptname)
{
	CScriptBuilder scriptBuilder;
	FILE* f = fopen(scriptname.c_str(), "rb");
	if(!f)
	{
		std::cout << "Failed to open " + scriptname + "!\n";
		return -1;
	}

	fseek(f, 0, SEEK_END);
	int len = ftell(f);
	fseek(f, 0, SEEK_SET);

	std::string script;
	script.resize(len);
	size_t c = fread(&script[0], len, 1, f);
	fclose(f);

	if(!c) return -1;

	int r = scriptBuilder.StartNewModule(asEngine, "script");

	if(r < 0) return -1;

	r = scriptBuilder.AddSectionFromFile(scriptname.c_str());

	if(r < 0) return -1;

	r = scriptBuilder.BuildModule();

	if(r < 0) return -1;

	return 0;
}

int RunScript(asIScriptEngine* asEngine) {
	int r;	
	asIScriptContext* ctx = asEngine->CreateContext();

	if(!ctx)
	{
		asEngine->Release();
		return -1;
	}

	asIScriptModule* mod = asEngine->GetModule("script", asGM_ONLY_IF_EXISTS);
	if(!mod) return -1;

	asIScriptFunction* func = mod->GetFunctionByDecl("void main()");

	if(!func)
	{
		std::cout << "There is no main function!\n";
		asEngine->Release();
		return -1;
	}

	ctx->Prepare(func);
	r = ctx->Execute();

	ctx->Release();

	return 0;
}

int main (int argc, char* argv[])
{
	std::string scriptname;

	if (argc < 2) 
	{
 		scriptname = "script.as";
 	}
	else
	{
		scriptname = argv[argc - 1];
	}

	srand(time(NULL));
	asIScriptEngine* asEngine = asCreateScriptEngine();
	
	ConfigEngine(asEngine);
	int r = CompileScript(asEngine, scriptname);

	if(r < 0)
	{
		std::cout << "Couldn't compile the script!\n";
		asEngine->Release();
		return -1;
	}
	RunScript(asEngine);
	asEngine->ShutDownAndRelease();

	return 0;
}
