#include 'person.as'

void main()
{
	Person George = Person("George");
	Person Jake = Person("Jake");
	George.Greet(Jake);
	Jake.Greet(George);
}
