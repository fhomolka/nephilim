void main()
{
	const uint TRIANGLE_HEIGHT = 5;
	string triangle_output;

	for(uint i = 0; i < TRIANGLE_HEIGHT; i++)
	{
		triangle_output += "|";
		for(uint j = 0; j < i; j++)
		{
			if(i != TRIANGLE_HEIGHT - 1)
			{
				triangle_output += " ";
			}
			else
			{
				triangle_output += "_";
			}
		}
		triangle_output += "\\\n";
	}
	print(triangle_output);
}
