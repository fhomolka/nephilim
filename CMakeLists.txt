cmake_minimum_required(VERSION 3.10.0)
project(Nephilim VERSION 0.1.0)

set(AS_DIR src/thirdparty/angelscript_sdk)

add_subdirectory(${AS_DIR}/angelscript/projects/cmake)

file(GLOB AS_EXT_SRC 
	${AS_DIR}/add_on/scriptstdstring/scriptstdstring.cpp
	${AS_DIR}/add_on/scriptarray/scriptarray.cpp
	${AS_DIR}/add_on/scriptdictionary/scriptdictionary.cpp
	${AS_DIR}/add_on/scriptmath/scriptmath.cpp
	${AS_DIR}/add_on/scriptfile/scriptfile.cpp
	${AS_DIR}/add_on/scriptbuilder/scriptbuilder.cpp
	#${AS_DIR}/add_on/scriptstdstring/scriptstdstring_utils.cpp
	)

add_executable(${PROJECT_NAME} src/main.cpp
								src/builtins.cpp
								${AS_EXT_SRC}
								${ANGELSCRIPT_SOURCE}
	)

target_include_directories(${PROJECT_NAME} PUBLIC ${AS_DIR}/angelscript/include)
target_include_directories(${PROJECT_NAME} PUBLIC ${AS_DIR}/add_on)

target_link_directories(${PROJECT_NAME} PUBLIC ${AS_DIR}/angelscript/lib)

target_link_libraries(${PROJECT_NAME} angelscript)
