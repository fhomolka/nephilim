class Rule
{
	string Word;
	int Number;

	Rule()
	{}
	Rule(int number, string word)
	{
		this.Number = number;
		this.Word = word;
	}
}

void main()
{
	array<Rule> rules =
	{
		Rule(3, "Fizz"),
		Rule(5, "Buzz")
	};

	string output;

	for(int i = 1; i <= 100; i++)
	{
		output = "";

		for (uint j = 0; j < rules.length(); j++)
		{
			if(i % rules[j].Number == 0)
			{
				output += rules[j].Word;
			}
		}

		if(output == "")
		{
			output = formatInt(i, 'l', 3);
		}

		print(output);
		print("\n");
	}
}
