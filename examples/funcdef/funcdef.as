funcdef void voidfunc();

void A()
{
	print("Hello from func A\n");
}

void B()
{
	print("Hello from func B\n");
}

void main()
{
	voidfunc @f;
	
	@f = A;	
	f();

	@f = B;
	f();
}
